//
//  CredentialsStoreTest.swift
//  LibraryTests
//
//  Created by Yan Smaliak on 29.01.2021.
//

import Foundation
@testable import Library

final class CredentialsStoreTest: CredentialsStoreType {
    var userCredentials: UserCredentials?
}
