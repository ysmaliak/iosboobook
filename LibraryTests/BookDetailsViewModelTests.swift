//
//  BookDetailsViewModelTests.swift
//  LibraryTests
//
//  Created by Yan Smaliak on 29.01.2021.
//

import XCTest
import RxSwift
import RxCocoa
import RxBlocking
@testable import Library

final class BookDetailsViewModelTests: XCTestCase {
    
    func testTransform() {
        let router = BookDetailsRouterTest()
        let repository = BookDetailsRepositoryTest()
        
        let viewModel = BookDetailsViewModel(
            router: router,
            repository: repository,
            bookID: Book.empty.payload.id
        )
        
        let output = viewModel.transform(
            BookDetailsViewModelInput(
                refreshTrigger: Driver.from(optional: ()),
                shareButton: Driver.from(optional: ()),
                takeButton: Driver.from(optional: ())
            )
        )
        
        XCTAssertEqual(try output.buttonTitle.toBlocking(timeout: 3).last()!, L10n.Localizable.reserve)
        XCTAssert(type(of: try output.cover.toBlocking(timeout: 3).last()!) == BookCover.self)
        XCTAssertTrue(try output.isButtonEnabled.toBlocking(timeout: 3).first()!)
        XCTAssertFalse(try output.isLoading.toBlocking(timeout: 3).first()!)
        XCTAssertNil(try output.myBook.toBlocking(timeout: 3).last()!)
        XCTAssertEqual(try output.name.toBlocking(timeout: 3).last()!, Book.empty.payload.name)
    }
    
}

final class BookDetailsRouterTest: BaseRouterTest, BookDetailsRouterType {
    func shareBook(named name: String, withCover cover: UIImage) {
        debugPrint("Share book named: \(name)")
    }
    
    func showAlert(withMessage message: String, andImage image: UIImage) {
        debugPrint("Show alert")
    }
}

final class BookDetailsRepositoryTest: BookDetailsRepositoryType {
    func loadBook(withID bookID: BookID) -> Single<Book> {
        .create { single in
            let disposable = Disposables.create()
            single(.success(.empty))
            return disposable
        }
    }
    
    func loadBookFromStorage(withID bookID: BookID) -> Single<Book> {
        .create { single in
            let disposable = Disposables.create()
            single(.success(.empty))
            return disposable
        }
    }
    
    func takeOrReturnBookAction(forBookWithID bookID: BookID, andReaderUserID readerUserID: UserID?) -> Single<Book> {
        .create { single in
            let disposable = Disposables.create()
            single(.success(.empty))
            return disposable
        }
    }
    
    func bookBeingReadByCurrentUserFromStorage() -> Single<Book?> {
        .create { single in
            let disposable = Disposables.create()
            single(.success(nil))
            return disposable
        }
    }
}
