//
//  RequestTests.swift
//  LibraryTests
//
//  Created by Yan Smaliak on 27.01.2021.
//

import XCTest
@testable import Library

final class RequestTests: XCTestCase {
    
    let encoder = JSONEncoder()
    let sharedHeaders = ["Content-Type": "application/json"]
    
    func testLogin() throws {
        let credentials = Credentials(mail: "mail", password: "password")
        let request = Request<UserData>.login(credentials: credentials)
        
        XCTAssertEqual(request.httpMethod, .post, "Wrong HTTP method")
        XCTAssertEqual(request.endpoint, "login", "Wrong endpoint")
        XCTAssertFalse(request.isAuthorizationNeeded, "Wrong 'isAuthorizationNeeded' value")
        XCTAssertEqual(request.headers, sharedHeaders, "Wrong HTTP headers")
        XCTAssertNoThrow(try encoder.encode(request.body?.asAnyEncodable), "Cannot encode body")
        XCTAssertEqual(request.queryItems, [:], "Wrong query items")
    }
    
    func testSignup() throws {
        let credentials = Credentials(mail: "mail", password: "password")
        let request = Request<UserData>.signup(credentials: credentials)
        
        XCTAssertEqual(request.httpMethod, .post, "Wrong HTTP method")
        XCTAssertEqual(request.endpoint, "users", "Wrong endpoint")
        XCTAssertFalse(request.isAuthorizationNeeded, "Wrong 'isAuthorizationNeeded' value")
        XCTAssertEqual(request.headers, sharedHeaders, "Wrong HTTP headers")
        XCTAssertNoThrow(try encoder.encode(request.body?.asAnyEncodable), "Cannot encode body")
        XCTAssertEqual(request.queryItems, [:], "Wrong query items")
    }
    
    func testAllBooks() throws {
        let request = Request<[PayloadBook]>.allBooks()
        
        XCTAssertEqual(request.httpMethod, .get, "Wrong HTTP method")
        XCTAssertEqual(request.endpoint, "books", "Wrong endpoint")
        XCTAssertTrue(request.isAuthorizationNeeded, "Wrong 'isAuthorizationNeeded' value")
        XCTAssertEqual(request.headers, sharedHeaders, "Wrong HTTP headers")
        XCTAssertNil(request.body, "Body in not nil")
        XCTAssertEqual(request.queryItems, [:], "Wrong query items")
    }
    
    func testBookByID() throws {
        let bookID = 1
        let request = Request<PayloadBook>.bookByID(bookID: bookID)
        
        XCTAssertEqual(request.httpMethod, .get, "Wrong HTTP method")
        XCTAssertEqual(request.endpoint, "books/\(bookID)", "Wrong endpoint")
        XCTAssertTrue(request.isAuthorizationNeeded, "Wrong 'isAuthorizationNeeded' value")
        XCTAssertEqual(request.headers, sharedHeaders, "Wrong HTTP headers")
        XCTAssertNil(request.body, "Body in not nil")
        XCTAssertEqual(request.queryItems, [:], "Wrong query items")
    }
    
    func testBookBeingRead() throws {
        let userID = 1
        let request = Request<PayloadBook?>.bookBeingRead(by: userID)
        
        XCTAssertEqual(request.httpMethod, .get, "Wrong HTTP method")
        XCTAssertEqual(request.endpoint, "book/user_read", "Wrong endpoint")
        XCTAssertTrue(request.isAuthorizationNeeded, "Wrong 'isAuthorizationNeeded' value")
        XCTAssertEqual(request.headers, sharedHeaders, "Wrong HTTP headers")
        XCTAssertNil(request.body, "Body in not nil")
        XCTAssertEqual(request.queryItems, ["id": "\(userID)"], "Wrong query items")
    }
    
    func testAddBook() throws {
        let bookName = BookName(name: "name")
        let request = Request<PayloadBook>.addBook(withName: bookName)
        
        XCTAssertEqual(request.httpMethod, .post, "Wrong HTTP method")
        XCTAssertEqual(request.endpoint, "books", "Wrong endpoint")
        XCTAssertTrue(request.isAuthorizationNeeded, "Wrong 'isAuthorizationNeeded' value")
        XCTAssertEqual(request.headers, sharedHeaders, "Wrong HTTP headers")
        XCTAssertNoThrow(try encoder.encode(request.body?.asAnyEncodable), "Cannot encode body")
        XCTAssertEqual(request.queryItems, [:], "Wrong query items")
    }
    
    func testReserveBook() throws {
        let bookID = 1
        let request = Request<PayloadBook>.reserveBook(withId: bookID)
        
        XCTAssertEqual(request.httpMethod, .put, "Wrong HTTP method")
        XCTAssertEqual(request.endpoint, "books/reserve/\(bookID)", "Wrong endpoint")
        XCTAssertTrue(request.isAuthorizationNeeded, "Wrong 'isAuthorizationNeeded' value")
        XCTAssertEqual(request.headers, sharedHeaders, "Wrong HTTP headers")
        XCTAssertNil(request.body, "Body in not nil")
        XCTAssertEqual(request.queryItems, [:], "Wrong query items")
    }
    
    func testReturnBook() throws {
        let bookID = 1
        let request = Request<PayloadBook>.returnBook(withId: bookID)
        
        XCTAssertEqual(request.httpMethod, .put, "Wrong HTTP method")
        XCTAssertEqual(request.endpoint, "books/return/\(bookID)", "Wrong endpoint")
        XCTAssertTrue(request.isAuthorizationNeeded, "Wrong 'isAuthorizationNeeded' value")
        XCTAssertEqual(request.headers, sharedHeaders, "Wrong HTTP headers")
        XCTAssertNil(request.body, "Body in not nil")
        XCTAssertEqual(request.queryItems, [:], "Wrong query items")
    }
    
}
