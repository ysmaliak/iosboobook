//
//  Book+ExtensionTest.swift
//  LibraryTests
//
//  Created by Yan Smaliak on 29.01.2021.
//

import Foundation
@testable import Library

extension Book {
    static let empty = Book(
        payload: .empty,
        isBeingReadByCurrentUser: false,
        isOwnedByCurrentUser: false
    )
}
