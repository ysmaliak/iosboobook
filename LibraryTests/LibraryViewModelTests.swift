//
//  LibraryViewModelTests.swift
//  LibraryTests
//
//  Created by Yan Smaliak on 29.01.2021.
//

import XCTest
import RxSwift
import RxCocoa
import RxBlocking
@testable import Library

final class LibraryViewModelTests: XCTestCase {
    
    func testTransform() {
        let router = LibraryRouterTest()
        let repository = LibraryRepositoryTest()
        
        let viewModel = LibraryViewModel(
            router: router,
            repository: repository
        )
        
        let output = viewModel.transform(
            LibraryViewModelInput(refreshTrigger: Driver.from(optional: ()),
                                  addBookButton: Driver.from(optional: ()),
                                  modeButton: Driver.from(optional: ()),
                                  selectedCell: Driver.from(optional: BookCellModel(book: .empty)))
        )
        
        XCTAssertEqual(try output.booksCountLabel.toBlocking(timeout: 3).last()!, "1 total, 0 of my")
        XCTAssertEqual(try output.cellsModels.toBlocking(timeout: 3).last()!.count, 1)
        XCTAssertEqual(try output.cellsModels.toBlocking(timeout: 3).last()!.first?.bookID, Book.empty.payload.id)
        XCTAssertFalse(try output.isLoading.toBlocking(timeout: 3).first()!)
        XCTAssertEqual(try output.modeTypeLabel.toBlocking(timeout: 3).first()!, L10n.Localizable.all.uppercased())
    }
    
}

final class LibraryRouterTest: BaseRouterTest, LibraryRouterType {
    func showAddBookAlert() -> Single<BookName> {
        .create { single in
            let disposable = Disposables.create()
            single(.success(BookName(name: "name")))
            return disposable
        }
    }
    
    func showBookModeSelectionMenu() -> Single<BooksShowModeType> {
        .create { single in
            let disposable = Disposables.create()
            single(.success(.all))
            return disposable
        }
    }
    
    func showDetails(forBookWithID bookID: BookID) {
        debugPrint("Show details for book with id: \(bookID)")
    }
}

final class LibraryRepositoryTest: LibraryRepositoryType {
    func allBooks() -> Single<[Book]> {
        .create { single in
            let disposable = Disposables.create()
            single(.success([.empty]))
            return disposable
        }
    }
    
    func allBooksFromStorage() -> Single<[Book]> {
        .create { single in
            let disposable = Disposables.create()
            single(.success([.empty]))
            return disposable
        }
    }
    
    func addBook(withName: BookName) -> Single<Book> {
        .create { single in
            let disposable = Disposables.create()
            single(.success(.empty))
            return disposable
        }
    }
}
