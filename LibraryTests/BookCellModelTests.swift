//
//  BookCellModelTests.swift
//  LibraryTests
//
//  Created by Yan Smaliak on 29.01.2021.
//

import XCTest
@testable import Library

final class BookCellModelTests: XCTestCase {

    func testInit() {
        let model = BookCellModel(book: Book.empty)
        
        XCTAssertEqual(model.bookID, Book.empty.payload.id)
        XCTAssertEqual(model.bookStatus, Book.empty.payload.status.readable)
        XCTAssert(type(of: model.cover) == BookCover.self)
        XCTAssertNil(model.myBook)
        XCTAssertNil(model.readingNow)
        XCTAssertEqual(model.title, Book.empty.payload.name)
    }
    
    func testReadingNow() {
        let viewModel = BookCellModel(book: Book(payload: .empty, isBeingReadByCurrentUser: true, isOwnedByCurrentUser: false))
        
        XCTAssertEqual(viewModel.readingNow, L10n.Localizable.readingNow)
    }

}
