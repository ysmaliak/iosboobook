//
//  ApiControllerTest.swift
//  LibraryTests
//
//  Created by Yan Smaliak on 29.01.2021.
//

import Foundation
import RxSwift
@testable import Library

final class ApiControllerTest: ApiControllerType {
    func call<ResponseType>(_ request: Request<ResponseType>) -> Single<ResponseType> where ResponseType: Decodable {
        .create { single in
            let disposable = Disposables.create()
            single(.error(ResponseError.badStatusCode))
            return disposable
        }
    }
}
