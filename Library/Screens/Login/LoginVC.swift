//
//  LoginVC.swift
//  Library
//
//  Created by Yan Smaliak on 11/11/20.
//

import UIKit
import RxSwift
import RxCocoa

final class LoginVC: UIViewController {
    
    // MARK: - Properties
    
    private let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.contentSize = CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        return scrollView
    }()
    
    private let emailTextField: TextField = {
        let textField = TextField()
        textField.placeholder = L10n.Localizable.email
        textField.autocapitalizationType = .none
        textField.keyboardType = .emailAddress
        textField.autocorrectionType = .no
        return textField
    }()
    
    private let passwordTextField: TextField = {
        let textField = TextField()
        textField.placeholder = L10n.Localizable.password
        textField.autocapitalizationType = .none
        textField.isSecureTextEntry = true
        textField.autocorrectionType = .no
        return textField
    }()
    
    private let loginButton = Button(withTitle: L10n.Localizable.login)
    
    private let signupButton = Button(withTitle: L10n.Localizable.signUp)
    
    private let logoView: UIImageView = {
        let logoView = UIImageView()
        logoView.translatesAutoresizingMaskIntoConstraints = false
        logoView.image = UIImage(systemName: "books.vertical.fill",
                                 withConfiguration: UIImage.SymbolConfiguration(pointSize: 96))?
            .withTintColor(.label, renderingMode: .alwaysOriginal)
        return logoView
    }()
    
    private let loadingView = LoadingView()
    
    private var disposeBag = DisposeBag()
    
    var viewModel: LoginViewModelType? {
        didSet {
            loadViewIfNeeded()
            bindViewModel()
        }
    }
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupLayout()
    }
}

// MARK: - Private Extension

private extension LoginVC {
    
    func bindViewModel() {
        disposeBag = .init()
        
        guard let output = viewModel?.transform(
            .init(email: emailTextField.rx.text.asDriver(),
                  password: passwordTextField.rx.text.asDriver(),
                  loginButtonTapped: loginButton.rx.tap.asDriver(),
                  signupButtonTapped: signupButton.rx.tap.asDriver(),
                  doneTrigger: .merge(
                    emailTextField.rx.controlEvent(.editingDidEndOnExit).asDriver(),
                    passwordTextField.rx.controlEvent(.editingDidEndOnExit).asDriver()
                  ))
        ) else { return }
        
        disposeBag.insert(
            output.isLoading.drive(onNext: { [unowned self] in
                loadingView.isPresented = $0
            }),
            output.isButtonEnabled.drive(onNext: { [unowned self] in
                loginButton.isActive = $0
                signupButton.isActive = $0
            }),
            output.triggers.drive(),
            output.endEditing.drive(onNext: { [weak self] in self?.view.endEditing(true) }),
            output.keyboardHeight.drive(onNext: { [scrollView] keyboardVisibleHeight in
                                    scrollView.contentOffset.y += keyboardVisibleHeight
                                    scrollView.contentInset.bottom = keyboardVisibleHeight
            })
        )
    }
    
    func setupLayout() {
        view.backgroundColor = .systemBackground
        
        setViews()
        setConstraints()
    }
    
    func setViews() {
        view.addSubview(scrollView)
        scrollView.addSubview(emailTextField)
        scrollView.addSubview(passwordTextField)
        scrollView.addSubview(loginButton)
        scrollView.addSubview(signupButton)
        scrollView.addSubview(logoView)
        view.addSubview(loadingView)
    }
    
    func setConstraints() {
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: view.topAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            
            logoView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor),
            logoView.centerYAnchor.constraint(equalTo: scrollView.topAnchor, constant: UIScreen.main.bounds.height/4),
            
            emailTextField.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor),
            emailTextField.centerYAnchor.constraint(equalTo: logoView.bottomAnchor, constant: UIScreen.main.bounds.height/4),
            emailTextField.heightAnchor.constraint(equalToConstant: emailTextField.defaultHeight),
            emailTextField.widthAnchor.constraint(equalToConstant: emailTextField.defaultWidth),
            
            passwordTextField.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor),
            passwordTextField.topAnchor.constraint(equalTo: emailTextField.bottomAnchor, constant: 10),
            passwordTextField.heightAnchor.constraint(equalToConstant: passwordTextField.defaultHeight),
            passwordTextField.widthAnchor.constraint(equalToConstant: passwordTextField.defaultWidth),
            
            loginButton.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor),
            loginButton.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: UIScreen.main.bounds.height/2 - 275),
            loginButton.heightAnchor.constraint(equalToConstant: loginButton.defaultHeight),
            loginButton.widthAnchor.constraint(equalToConstant: loginButton.defaultWidth),
            
            signupButton.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor),
            signupButton.topAnchor.constraint(equalTo: loginButton.bottomAnchor, constant: 10),
            signupButton.heightAnchor.constraint(equalToConstant: signupButton.defaultHeight),
            signupButton.widthAnchor.constraint(equalToConstant: signupButton.defaultWidth)
        ])
    }
}
