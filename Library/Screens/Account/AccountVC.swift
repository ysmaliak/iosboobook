//
//  AccountVC.swift
//  Library
//
//  Created by Yan Smaliak on 07.12.2020.
//

import UIKit
import RxSwift
import RxCocoa

final class AccountVC: UIViewController {
    
    // MARK: - Properties
    
    private let logoutButton: UIBarButtonItem = {
        let button = UIBarButtonItem(title: L10n.Localizable.logOut, style: .plain, target: nil, action: nil)
        button.tintColor = .label
        return button
    }()
    
    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.estimatedSectionHeaderHeight = 88
        tableView.sectionHeaderHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 88
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView()
        tableView.isScrollEnabled = false
        return tableView
    }()
    
    private let spinner: UIActivityIndicatorView = {
        let spinner = UIActivityIndicatorView()
        spinner.translatesAutoresizingMaskIntoConstraints = false
        spinner.style = .large
        return spinner
    }()
    
    private let headerView = AccountHeaderView()
    
    private var disposeBag = DisposeBag()
    
    var viewModel: AccountViewModelType? {
        didSet {
            loadViewIfNeeded()
            bindViewModel()
        }
    }
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTableView()
        setupLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !view.subviews.contains(tableView) {
            embedTableView()
        }
        if !view.subviews.contains(spinner) {
            embedSpinner()
        }
    }
}

// MARK: - TableView Delegate

extension AccountVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return headerView
    }
}

// MARK: - UIAdaptivePresentationControllerDelegate

extension AccountVC: UIAdaptivePresentationControllerDelegate {
    func presentationControllerDidDismiss(_ presentationController: UIPresentationController) {}
}

// MARK: - Private Extension

private extension AccountVC {
    func setupLayout() {
        navigationItem.title = L10n.Localizable.account
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.rightBarButtonItem = logoutButton
        view.backgroundColor = .systemBackground
    }
    
    func embedSpinner() {
        view.addSubview(spinner)
        
        NSLayoutConstraint.activate([
            spinner.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            spinner.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
    }
    
    func embedTableView() {
        view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor)
        ])
    }
    
    func setupTableView() {
        tableView.delegate = self
        tableView.register(cellType: BookCell.self)
    }
    
    func bindViewModel() {
        disposeBag = .init()
        
        let viewWillAppear = rx
            .sentMessage(#selector(UIViewController.viewWillAppear(_:)))
            .asDriver(onErrorDriveWith: .never())
            .mapToVoid()
        
        let modalDismissed = rx
            .sentMessage(#selector(UIAdaptivePresentationControllerDelegate.presentationControllerDidDismiss(_:)))
            .asDriver(onErrorDriveWith: .never())
            .mapToVoid()
        
        guard let output = viewModel?.transform(.init(refreshTrigger: .merge(viewWillAppear, modalDismissed),
                                                      logoutButton: logoutButton.rx.tap.asDriver(),
                                                      selectedCell: tableView.rx.modelSelected(BookCellModel.self).asDriver())
        ) else { return }
        
        disposeBag.insert(
            output.emailLabel.drive(onNext: { [unowned self] result in
                headerView.emailLabelText = result
            }),
            output.cellModel.drive(tableView.rx.items(cellIdentifier: BookCell.reuseIdentifier,
                                                          cellType: BookCell.self)) { (_, cellViewModel, cell) in
                cell.viewModel = cellViewModel
            },
            output.isLoading.drive(spinner.rx.isAnimating),
            output.triggers.drive()
        )
    }
}
