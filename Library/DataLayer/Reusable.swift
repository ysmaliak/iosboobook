//
//  Reusable.swift
//  Library
//
//  Created by Yan Smaliak on 30.11.2020.
//

import Foundation

public protocol Reusable: class {
    static var reuseIdentifier: String { get }
}

public extension Reusable {
    static var reuseIdentifier: String {
        String(describing: self)
    }
}
