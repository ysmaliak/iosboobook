//
//  Credentials.swift
//  Library
//
//  Created by Yan Smaliak on 16.11.2020.
//

import Foundation

/// Used to make signup/login calls
struct Credentials: Encodable {
    let email: String
    let password: String
}
