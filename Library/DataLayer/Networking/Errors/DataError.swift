//
//  DataError.swift
//  Library
//
//  Created by Yan Smaliak on 16.11.2020.
//

import Foundation

enum DataError: Error {
    case cannotEncode
}
