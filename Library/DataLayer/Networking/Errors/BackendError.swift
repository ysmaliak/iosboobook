//
//  BackendError.swift
//  Library
//
//  Created by Yan Smaliak on 27.01.2021.
//

import Foundation

struct BackendError: Error {
    let payload: Data
}
