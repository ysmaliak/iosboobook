//
//  Response.swift
//  Library
//
//  Created by Yan Smaliak on 11/12/20.
//

import Foundation

struct Response<T: Decodable>: Decodable {
    let code: Int
    let status: String
    let data: T
}
