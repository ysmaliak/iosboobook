//
//  UserData.swift
//  Library
//
//  Created by Yan Smaliak on 11/12/20.
//

import Foundation

struct UserData: Decodable {
    let id: Int
    let email: String
    let accessToken: String
}

struct UserModel: Decodable {
    let bonuses: Double
}
