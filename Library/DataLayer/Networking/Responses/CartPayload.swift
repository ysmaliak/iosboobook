//
//  CartPayload.swift
//  Library
//
//  Created by Yan Smaliak on 13.11.2021.
//

import Foundation

struct CartPayload: Decodable {
    let userId: Int
    let books: [PayloadBook]

    enum CodingKeys: CodingKey {
        case userId
        case books
    }
}
