//
//  RequestParameters.swift
//  Library
//
//  Created by Yan Smaliak on 14.11.2020.
//

import Foundation

struct Request<ResponseType: Decodable> {
    var httpMethod: HTTPMethod = .get
    let endpoint: String
    var isAuthorizationNeeded = true
    var headers: [String: String] = ["Content-Type": "application/json"]
    var body: Encodable?
    var queryItems: [String: String] = [:]
}

extension Request {
    static func login(credentials: Credentials) -> Request<UserData> {
		.init(
			httpMethod: .post,
			endpoint: "users/login",
			isAuthorizationNeeded: false,
			queryItems: [
				"email": credentials.email,
				"password": credentials.password
			]
		)
	}
    
    static func signup(credentials: Credentials) -> Request<UserData> {
		return .init(
			httpMethod: .post,
			endpoint: "users",
			isAuthorizationNeeded: false,
			queryItems: [
				"email": credentials.email,
				"password": credentials.password
			]
		)
    }
    
    static func allBooks() -> Request<[PayloadBook]> {
        return .init(endpoint: "books")
    }
    
    static func bookByID(bookID: BookID) -> Request<PayloadBook> {
        return .init(endpoint: "books/\(bookID)")
    }
    
    static func addBook(withName bookName: BookName) -> Request<PayloadBook> {
        return .init(httpMethod: .post,
                     endpoint: "books",
                     body: bookName)
    }
    
    static func reserveBook(withId bookID: BookID) -> Request<PayloadBook> {
        return .init(httpMethod: .put,
                     endpoint: "books/reserve/\(bookID)")
    }
    
    static func returnBook(withId bookID: BookID) -> Request<PayloadBook> {
        return .init(httpMethod: .put,
                     endpoint: "books/return/\(bookID)")
    }

    static func cart() -> Request<CartPayload> {
        return .init(endpoint: "cart")
    }

    static func addToCart(bookWithId bookId: BookID) -> Request<CartPayload> {
        return .init(
            httpMethod: .post,
            endpoint: "cart/\(bookId)"
        )
    }

    static func removeAllFromCart(bookWithId bookId: BookID) -> Request<CartPayload> {
        return .init(
            httpMethod: .delete,
            endpoint: "cart/remove/\(bookId)"
        )
    }

    static func removeFromCart(bookWithId bookId: BookID) -> Request<CartPayload> {
        return .init(
            httpMethod: .delete,
            endpoint: "cart/\(bookId)"
        )
    }

    static func order(_ order: Order) -> Request<CartPayload> {
        return .init(
            httpMethod: .post,
            endpoint: "cart/order",
            queryItems: [
                "Bonuses": "\(order.bonuses)",
                "Name": order.name,
                "Phone": order.number,
                "Address.City": order.city,
                "Address.Street": order.street,
                "Address.House": order.house,
                "Address.Flat": order.flat,
                "Address.Code": "\(order.code)"
            ]
        )
    }

    static func bonuses() -> Request<UserModel> {
        .init(endpoint: "users/user")
    }
}

struct OrderStatus {
    let status: String
}
