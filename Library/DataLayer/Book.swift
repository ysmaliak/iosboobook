//
//  Book.swift
//  Library
//
//  Created by Yan Smaliak on 18.01.2021.
//

import Foundation
import UIKit

struct Book {
    let payload: PayloadBook
    let isBeingReadByCurrentUser: Bool
    let isOwnedByCurrentUser: Bool

    var image: UIImage? {
        if let imageString = payload.image {
            let imageData = Data.init(base64Encoded: imageString, options: .init(rawValue: 0))
            let image = UIImage(data: imageData!)
            return image
        }

        return nil
    }
}
