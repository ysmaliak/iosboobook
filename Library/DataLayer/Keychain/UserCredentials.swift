//
//  UserCredentials.swift
//  Library
//
//  Created by Yan Smaliak on 05.01.2021.
//

import Foundation

typealias UserID = Int

/// Credentials used to store in 'CredentialsStoreType'
struct UserCredentials {
    let id: UserID
    let email: String
    let token: String
}
