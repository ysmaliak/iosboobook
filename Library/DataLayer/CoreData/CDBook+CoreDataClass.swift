//
//  CDBook+CoreDataClass.swift
//  Library
//
//  Created by Yan Smaliak on 12.01.2021.
//
//

import Foundation
import CoreData

@objc(CDBook)
final public class CDBook: NSManagedObject {
    @nonobjc public class func createFetchRequest() -> NSFetchRequest<CDBook> {
        return NSFetchRequest<CDBook>(entityName: "CDBook")
    }
    
    @NSManaged public var createdAt: Date
    @NSManaged public var id: Int32
    @NSManaged public var name: String
    @NSManaged public var updatedAt: Date
    @NSManaged public var image: String?
    @NSManaged public var price: Double
    @NSManaged public var bookDescription: String?
    @NSManaged public var author: String
    @NSManaged public var count: Int16
        
    var book: PayloadBook {
		return PayloadBook(
			id: BookID(id),
			name: name,
			createdAt: createdAt,
			updatedAt: updatedAt,
            image: image,
            price: price,
            description: bookDescription,
            author: author,
            count: Int(count)
		)
    }
}
