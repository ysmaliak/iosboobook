//
//  CoreDataError.swift
//  Library
//
//  Created by Yan Smaliak on 19.01.2021.
//

import Foundation

enum CoreDataError: Error {
    case bookIsNotInStorage
}
