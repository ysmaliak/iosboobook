//
//  MainRouter.swift
//  Library
//
//  Created by Yan Smaliak on 09.12.2020.
//

import UIKit

final class MainRouter {
    // MARK: - Properties
    
    var window: UIWindow?
    
    // MARK: - Public Methods
    
    func start() {
        if SwinjectContainer.container.resolve(CredentialsStoreType.self)!.userCredentials != nil {
            
            // MARK: TabBar
            
            let tabBarController = UITabBarController()
            
            tabBarController.tabBar.tintColor = .label
            
            let libraryVC = LibraryVC()
            let libraryNavigationController = UINavigationController(rootViewController: libraryVC)
            let libraryRouter = LibraryRouter(rootViewController: libraryVC)
            libraryVC.viewModel = LibraryViewModel(router: libraryRouter,
                                                   repository: SwinjectContainer.container.resolve(BookRepository.self)!)
            libraryNavigationController.tabBarItem.title = "Store"
            libraryNavigationController.tabBarItem.image = UIImage(systemName: "books.vertical")
			
			let cartVC = CartVC()
			let cartNavigationController = UINavigationController(rootViewController: cartVC)
			let cartRouter = CartRouter(rootViewController: cartVC)
			cartVC.viewModel = CartViewModel(
				router: cartRouter,
				repository: SwinjectContainer.container.resolve(BookRepository.self)!
			)
			cartNavigationController.tabBarItem.title = "Cart"
			cartNavigationController.tabBarItem.image = UIImage(systemName: "cart")
            
            let accountVC = AccountVC()
            let accountNavigationController = UINavigationController(rootViewController: accountVC)
            let accountRouter = AccountRouter(rootViewController: accountVC)
            accountVC.viewModel = AccountViewModel(router: accountRouter,
                                                   repository: SwinjectContainer.container.resolve(BookRepository.self)!,
                                                   currentUserEmail: SwinjectContainer.container.resolve(CredentialsStoreType.self)!
                                                    .userCredentials!.email,
                                                   logOut: logOut)
            accountNavigationController.tabBarItem.title = L10n.Localizable.account
            accountNavigationController.tabBarItem.image = UIImage(systemName: "person")
            
            tabBarController.viewControllers = [
				libraryNavigationController,
				cartNavigationController,
				accountNavigationController
			]
            
            window?.rootViewController = tabBarController
        } else {
            
            // MARK: Login
            
            let loginVC = LoginVC()
            let loginRouter = LoginRouter(rootViewController: loginVC, mainRouter: self)
            loginVC.viewModel = LoginViewModel(router: loginRouter,
                                               apiController: SwinjectContainer.container.resolve(ApiControllerType.self)!,
                                               credentialsStore: SwinjectContainer.container.resolve(CredentialsStoreType.self)!)
            
            window?.rootViewController = loginVC
        }
        
        window?.makeKeyAndVisible()
    }
    
    // MARK: - Private Methods
    
    private func logOut() {
        SwinjectContainer.container.resolve(CredentialsStoreType.self)!.userCredentials = nil
        start()
    }
}
