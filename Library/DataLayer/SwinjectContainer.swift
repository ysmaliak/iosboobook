//
//  SwinjectContainer.swift
//  Library
//
//  Created by Yan Smaliak on 05.01.2021.
//

import Foundation
import Swinject
import SwiftKeychainWrapper

final class SwinjectContainer {
    static let container: Container = {
        let container = Container()
		
		let appDelegate = UIApplication.shared.delegate as? AppDelegate
		let managedContext = appDelegate!.persistentContainer.viewContext
        
        container.register(CredentialsStoreType.self) { _ in KeychainCredentialsStore(keychain: KeychainWrapper.standard) }
        container.register(ApiControllerType.self) { _ in ApiController(credentialsStore: container.resolve(CredentialsStoreType.self)!) }
        container.register(BookStorageType.self) { _ in CoreDataBookStorage(managedContext: managedContext) }
        container.register(BookRepository.self) { _ in
            BookRepository(apiController: container.resolve(ApiControllerType.self)!,
                           bookStorage: container.resolve(BookStorageType.self)!,
                           currentUserID: container.resolve(CredentialsStoreType.self)!.userCredentials!.id)
        }
        
        return container
    }()
}
