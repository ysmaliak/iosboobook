//
//  Driver+Extension.swift
//  Library
//
//  Created by Yan Smaliak on 17.11.2020.
//

import RxCocoa

extension SharedSequence where SharingStrategy == DriverSharingStrategy {
    func mapToVoid() -> SharedSequence<DriverSharingStrategy, Void> {
        return map { _ in ()}
    }
}
