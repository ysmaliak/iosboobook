//
//  UIFont+Extension.swift
//  Library
//
//  Created by Yan Smaliak on 20.11.2020.
//

import UIKit

extension UIFont {
    func withTraits(traits: UIFontDescriptor.SymbolicTraits) -> UIFont {
        let descriptor = fontDescriptor.withSymbolicTraits(traits)
        return UIFont(descriptor: descriptor!, size: 0)
    }

    var bold: UIFont {
        return withTraits(traits: .traitBold)
    }
}
