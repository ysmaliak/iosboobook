//
//  ObservableConvertibleType+Driver.swift
//  Library
//
//  Created by Yan Smaliak on 26.01.2021.
//

import RxSwift
import RxCocoa

extension ObservableConvertibleType {
     func asDriver(onErrorDo: @escaping (_ error: Swift.Error) -> Void) -> Driver<Element> {
        self.asDriver(onErrorRecover: { error in
             onErrorDo(error)
             return .never()
         })
     }
 }
