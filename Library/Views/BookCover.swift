//
//  BookCover.swift
//  Library
//
//  Created by Yan Smaliak on 26.11.2020.
//

import UIKit

final class BookCover: UIView {
    
    // MARK: - Public Properties
    
    public var title: String {
        get {
            titleLabel.text ?? ""
        }
        set {
            titleLabel.text = newValue
        }
    }
    
    public var titleFont: UIFont {
        get {
            titleLabel.font
        }
        set {
            titleLabel.font = newValue
        }
    }

    public var baseImage: UIImage? = nil
    
    public func toImage(completion: @escaping (UIImage) -> Void) {
        let viewBounds = bounds
        titleLabel.frame = viewBounds
        let viewLayer = layer

        DispatchQueue.global(qos: .background).async {
            let renderer = UIGraphicsImageRenderer(bounds: viewBounds)
            let image = renderer.image { rendererContext in
                viewLayer.render(in: rendererContext.cgContext)
            }
            DispatchQueue.main.async {
                completion(image)
            }
        }
    }
    
    // MARK: - Private Properties
        
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: .caption1)
        label.textColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
    // MARK: - Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }

    private func commonInit(title: String, backgroundColor: UIColor) {
        commonInit()
        self.title = title
        self.backgroundColor = backgroundColor
    }
    
    private func commonInit() {
        layer.cornerRadius = 3
        layer.shadowColor = AppColors.shadow.cgColor
        layer.shadowOpacity = 1
        layer.shadowRadius = 3
        layer.shadowOffset = CGSize(width: 0, height: 3)
        translatesAutoresizingMaskIntoConstraints = false
        
        embedTitle()
    }
    
    // MARK: - Trait Collection
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        self.updateColors()
        self.setNeedsDisplay()
    }
    
    // MARK: - Private Methods
    
    private func embedTitle() {
        addSubview(titleLabel)
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: topAnchor),
            titleLabel.bottomAnchor.constraint(equalTo: bottomAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10 / UIScreen.main.scale),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10 / UIScreen.main.scale)
        ])
    }
    
    private func updateColors() {
        layer.shadowColor = AppColors.shadow.cgColor
    }
}
