//
//  AccountHeaderView.swift
//  Library
//
//  Created by Yan Smaliak on 07.12.2020.
//

import UIKit

final class AccountHeaderView: UIView {

    // MARK: - Public
    
    public var emailLabelText: String {
        get {
            return emailLabel.text ?? ""
        }
        set {
            emailLabel.text = newValue
        }
    }
    
    // MARK: - Private

    private let emailLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .label
        label.font = UIFont.preferredFont(forTextStyle: .subheadline).bold
        label.textAlignment = .left
        return label
    }()
    
    private let lastPurchaseLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .label
        label.font = UIFont.preferredFont(forTextStyle: .title2).bold
        label.textAlignment = .left
        label.text = ""
        return label
    }()
    
    private let topSeparatorView = SeparatorView()
    
    private let bottomSeparatorView = SeparatorView()
    
    // MARK: - Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        backgroundColor = .systemBackground
        
        addSubview(topSeparatorView)
        addSubview(emailLabel)
        addSubview(bottomSeparatorView)
        addSubview(lastPurchaseLabel)
        
        NSLayoutConstraint.activate([
            topSeparatorView.topAnchor.constraint(equalTo: topAnchor),
            topSeparatorView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15),
            topSeparatorView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15),
            
            emailLabel.topAnchor.constraint(equalTo: topSeparatorView.bottomAnchor, constant: 12),
            emailLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15),
            
            bottomSeparatorView.topAnchor.constraint(equalTo: emailLabel.bottomAnchor, constant: 12),
            bottomSeparatorView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15),
            bottomSeparatorView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15),
            
            lastPurchaseLabel.topAnchor.constraint(equalTo: bottomSeparatorView.bottomAnchor, constant: 12),
            lastPurchaseLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15),
            lastPurchaseLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -12)
        ])
    }
}
