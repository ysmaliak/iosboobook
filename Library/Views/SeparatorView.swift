//
//  SeparatorView.swift
//  Library
//
//  Created by Yan Smaliak on 20.11.2020.
//

import UIKit

final class SeparatorView: UIView {
    
    // MARK: - Public
    
    var isHorisontal = true {
        didSet { invalidateIntrinsicContentSize() }
    }
    
    // MARK: - Override
    
    override var intrinsicContentSize: CGSize {
        return .init(
            width: isHorisontal ? UIView.noIntrinsicMetric : 2 / UIScreen.main.scale,
            height: isHorisontal ? 2 / UIScreen.main.scale : UIView.noIntrinsicMetric
        )
    }
    
    // MARK: - Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        backgroundColor = .systemGray5
        translatesAutoresizingMaskIntoConstraints = false
    }
    
}
