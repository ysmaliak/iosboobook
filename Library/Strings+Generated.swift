// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name vertical_whitespace_opening_braces
internal enum L10n {
  internal enum InfoPlist {
    /// Library
    internal static let cfBundleDisplayName = L10n.tr("InfoPlist", "CFBundleDisplayName")
  }
  internal enum Localizable {
    /// Account
    internal static let account = L10n.tr("Localizable", "Account")
    /// Add Book
    internal static let addBook = L10n.tr("Localizable", "Add Book")
    /// ALL
    internal static let all = L10n.tr("Localizable", "ALL")
    /// Book name
    internal static let bookName = L10n.tr("Localizable", "Book name")
    /// Cancel
    internal static let cancel = L10n.tr("Localizable", "Cancel")
    /// Choose what you want to see
    internal static let chooseWhatYouWantToSee = L10n.tr("Localizable", "Choose what you want to see")
    /// Email
    internal static let email = L10n.tr("Localizable", "Email")
    /// Hey! Check out "%@" on Library App
    internal static func heyCheckOutOnLibraryApp(_ p1: Any) -> String {
      return L10n.tr("Localizable", "Hey! Check out %@ on Library App", String(describing: p1))
    }
    /// Library
    internal static let library = L10n.tr("Localizable", "Library")
    /// Log Out
    internal static let logOut = L10n.tr("Localizable", "Log Out")
    /// Login
    internal static let login = L10n.tr("Localizable", "Login")
    /// My Book
    internal static let myBook = L10n.tr("Localizable", "My Book")
    /// MY BOOKS
    internal static let myBooks = L10n.tr("Localizable", "MY BOOKS")
    /// of my
    internal static let ofMy = L10n.tr("Localizable", "of my")
    /// Ooops! Something went wrong!
    internal static let ooopsSomethingWentWrong = L10n.tr("Localizable", "Ooops! Something went wrong!")
    /// Password
    internal static let password = L10n.tr("Localizable", "Password")
    /// Reading Now
    internal static let readingNow = L10n.tr("Localizable", "Reading Now")
    /// Reserve
    internal static let reserve = L10n.tr("Localizable", "Reserve")
    /// Reserved
    internal static let reserved = L10n.tr("Localizable", "Reserved")
    /// Return
    internal static let `return` = L10n.tr("Localizable", "Return")
    /// Returned
    internal static let returned = L10n.tr("Localizable", "Returned")
    /// Save
    internal static let save = L10n.tr("Localizable", "Save")
    /// SHOW
    internal static let show = L10n.tr("Localizable", "SHOW")
    /// Sign Up
    internal static let signUp = L10n.tr("Localizable", "Sign Up")
    /// Status: %@
    internal static func status(_ p1: Any) -> String {
      return L10n.tr("Localizable", "Status: %@", String(describing: p1))
    }
    /// total
    internal static let total = L10n.tr("Localizable", "total")
    /// Uploaded at %@
    internal static func uploadedAt(_ p1: Any) -> String {
      return L10n.tr("Localizable", "Uploaded at %@", String(describing: p1))
    }
    /// Сheck your internet connection
    internal static let сheckYourInternetConnection = L10n.tr("Localizable", "Сheck your internet connection")
  }
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name vertical_whitespace_opening_braces

// MARK: - Implementation Details

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    let format = BundleToken.bundle.localizedString(forKey: key, value: nil, table: table)
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
